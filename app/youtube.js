var request = require('request');
var qs = require('qs');

module.exports = {
  // https://developers.google.com/youtube/v3/docs/search/list
	search: function(q, callback) {
    var baseUrl = 'https://www.googleapis.com/youtube/v3/search';
		var url = baseUrl + '?' + qs.stringify({
			part: 'snippet',
			q: q,
			key: 'AIzaSyBB2z956EzDg07Q0nMHdCbUoc7Xwo0kPrQ',
			type: 'playlist'
		});

		console.log(url);

		request(url, function(error, response, body) {
		  if (!error && response.statusCode == 200) {
		  	var items = JSON.parse(body).items;
		    callback(items);
		  }
		});
	},

  // https://developers.google.com/youtube/v3/docs/channels/list
  channels: function(callback) {
    var baseUrl = 'https://www.googleapis.com/youtube/v3/channels';
    var url = baseUrl + '?' + qs.stringify({
			part: 'id,snippet,statistics,contentDetails',
      forUsername: 'fouseyTube',
			key: 'AIzaSyBB2z956EzDg07Q0nMHdCbUoc7Xwo0kPrQ'
		});

		console.log(url);

		request(url, function(error, response, body) {
		  if (!error && response.statusCode == 200) {
		  	var items = JSON.parse(body).items;
		    callback(items);
		  }
		});
  }
};
