var request = require('request');
var qs = require('qs');
var config = require('./config');

var bearerToken;

/**
  What is Oauth? - A standard for authorization
  Two versions of OAuth: OAuth 1.0a and OAuth2 (more widely used)
  Authentication verifies who you are
  Authorization verifies what you are allowed to do. Authorization occurs after successful authentication.
*/
// https://dev.twitter.com/oauth/application-only
module.exports = {
  getBearerToken: function(callback) {
    if (bearerToken) {
      callback(bearerToken);
      return;
    }

    var apiKey = config.twitter.apiKey;
    var apiSecret = config.twitter.apiSecret;
    var compositeKey = apiKey + ':' + apiSecret;
    var base64 = new Buffer(compositeKey).toString('base64');
    // The Buffer class is a global type for dealing with binary data directly
    // base64 is for encoding binary data into characters (a string)

    request({
      method: 'POST',
      url: 'https://api.twitter.com/oauth2/token',
      headers: {
        Authorization: 'Basic ' + base64
      },
      form: {
        grant_type: 'client_credentials'
      }
    }, function(error, response, body) {
      console.log(body);
		  if (!error && response.statusCode == 200) {
		  	bearerToken = JSON.parse(body).access_token;
		    callback(bearerToken);
		  }
		});
  },

  getTweets: function(callback) {
    this.getBearerToken(function(token) {
      request({
        method: 'GET',
        url: 'https://api.twitter.com/1.1/statuses/user_timeline.json?' + qs.stringify({
          screen_name: 'uscitp',
          count: 10,
          exclude_replies: true
        }),
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }, function(error, response, body) {
  		  if (!error && response.statusCode == 200) {
  		  	var data = JSON.parse(body);
  		    callback(data);
  		  }
  		});
    });
  }
  
};
