var express = require('express');
var youtube = require('./app/youtube');
var twitter = require('./app/twitter');
var app = express();

app.use(express.static(__dirname + '/public'));

app.get('/youtube/search/:artist?', function(req, res) {
  var artist;

  if (req.params.artist) {
    artist = req.params.artist;
  } else {
    artist = req.query.artist;
  }

	youtube.search(artist, function(data) {
    res.json(data);
  });
});

app.get('/youtube/channels', function(req, res) {
	youtube.channels(function(data) {
    res.json(data);
  });
});

app.get('/tweets', function(req, res) {
  twitter.getTweets(function(tweets) {
    res.json(tweets);
  });
});

app.listen(3000, function() {
	console.log('Listening on port 3000');
});
