Express Demo
============

Configure where node puts global modules http://devin-clark.com/global-npm-install-without-sudo/

```
mkdir ~/npm
npm config set prefix ~/npm
```

Then add `PATH="$PATH:$HOME/npm/bin"` to ~/.bash_profile

* `npm install -g nodemon`
* [Getting an API Key](https://developers.google.com/youtube/v3/getting-started)
* [Youtube API Docs](https://developers.google.com/youtube/v3/docs/search)
* [Twitter API Docs](https://dev.twitter.com/overview/documentation)
